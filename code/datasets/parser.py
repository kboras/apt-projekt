import random
import csv

def parse_friends(input_file, train, dev, test):

    train = open(train, "w")
    test = open(test, "w")
    dev = open(dev, "w")

    for f in (train, test, dev):
        f.write("speaker\ttext\n")

    with open(input_file, newline='') as csvfile:
        spamreader = csv.reader(csvfile, delimiter=',', quotechar='"')
        for row in spamreader:
            speaker = row[4]
            # if speaker not in ("Joey", "Rachel", "Monica", "Ross", "Chandler", "Phoebe"):
            #     continue
            text = row[5]
            text = text.replace('"', '')
            where = random.randrange(0, 1000, 1)/1000
            tsv_format = speaker + "\t" + text + "\n"
            if 0 <= where <= 0.98:
                train.write(tsv_format)
            elif 0.98 < where <= 0.99:
                test.write(tsv_format)
            else:
                dev.write(tsv_format)



if __name__ == "__main__":

    parse_friends("friends_dataset.csv", "train.tsv", "dev.tsv", "test.tsv")