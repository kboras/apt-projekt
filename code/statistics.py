from scipy import stats

test_set = open("new_sets/test_set_results.txt", "r").readlines()
phrases_train = open("new_sets/phrases_train_results.txt", "r")

test_set_array = [float(line.strip()) for line in test_set]
phrases_train_array = [float(line.strip()) for line in phrases_train]

#  print(test_set_array)
#  print(phrases_train_array)

# performing the Kolmogorov-Smirnov normality test to see if the data follows a normal distribution (null hypothesis)
print(stats.kstest(test_set_array, 'norm'))
print(stats.kstest(phrases_train_array, 'norm'))

# performing the Shapiro-Wilk Test to see if the data follows a normal distribution (null hypothesis)
print(stats.shapiro(test_set_array))
print(stats.shapiro(phrases_train_array))

# performing the non-parametric Mann-Whitney U Test to see if the two samples are equal
print(stats.mannwhitneyu(test_set_array, phrases_train_array, use_continuity=False))
