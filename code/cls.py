import random
import numpy as np
from numpy import array
from podium import Field, Vocab, LabelField, TabularDataset
from podium.vectorizers import TfIdfVectorizer
from sklearn.metrics import classification_report, accuracy_score, multilabel_confusion_matrix, \
    precision_score, recall_score, f1_score
from sklearn.svm import SVC
import pickle
import matplotlib.pyplot

def evaluate(name, y_pred, y_true):

    print()
    print(name)
    print(classification_report(y_true, y_pred, zero_division=0, digits=5))
    print(multilabel_confusion_matrix(y_true, y_pred))
    string = " & ".join([str(round(accuracy_score(y_true, y_pred), 3)),
                         str(round(precision_score(y_true, y_pred, average="macro"), 3)),
                         str(round(recall_score(y_true, y_pred, average="macro"), 3)),
                         str(round(f1_score(y_true, y_pred, average="macro"), 3))])
    print(name, string)

    return accuracy_score(y_true, y_pred)

# DONT USE
def train_model(embeddings_x_train, y_train):

    global trained

    svm = SVC(C=1, random_state=42)
    svm.fit(embeddings_x_train, y_train.ravel())

    filename = 'models/finalized_model.sav'
    pickle.dump(svm, open(filename, 'wb'))


def cross_validate(X_train, y_train, X_test, y_test):

    scores = dict()

    for c in [1, 10]:
        for gamma in ["scale", "auto"]:
            svm = SVC(C=c, gamma=gamma)
            svm.fit(X_train, y_train)
            pred = svm.predict(X_test)
            acc = evaluate(str(c) + " " + str(gamma), pred, y_test)
            scores[acc] = (c, gamma)

    return scores[max(scores)]


def test_baselines(y_true):

    baseline2 = []
    baseline3 = []
    loudmouth = max(set(y_true.tolist()), key=y_true.tolist().count)

    for _ in y_true:
        rand_six = random.randint(0, 5)
        baseline2.append(rand_six)
        baseline3.append(loudmouth)

    baseline2 = array(baseline2)
    baseline3 = array(baseline3)

    evaluate("Baseline2", baseline2, y_true)
    evaluate("Baseline3", baseline3, y_true)


def create_datasets():

    vocab = Vocab()

    fields = {
        'text': Field('text', numericalizer=vocab, tokenizer=lambda x: x.split()),
        'speaker': LabelField('speaker')
    }

    train = TabularDataset("datasets/train.tsv", format="tsv", fields=fields.copy())
    train.finalize_fields()

    test = TabularDataset("datasets/test.tsv", format="tsv", fields=fields.copy())

    catch = TabularDataset("datasets/catchphrases.tsv", format="tsv", fields=fields.copy())

    phrases = TabularDataset("datasets/phrases.tsv", format="tsv", fields=fields.copy())
    phrases_train = TabularDataset("datasets/train_phrases1.tsv", format="tsv", fields=fields.copy())
    phrases_test = TabularDataset("datasets/test_phrases1.tsv", format="tsv", fields=fields.copy())

    x_train, y_train = train.batch()
    y_train = np.array(y_train).ravel()

    x_test, y_test = test.batch()
    y_test = np.array(y_test).ravel()

    x_catch, y_catch = catch.batch()
    y_catch = np.array(y_catch).ravel()

    x_phrases, y_phrases = phrases.batch()
    y_phrases = np.array(y_phrases).ravel()

    x_phrases_train, y_phrases_train = phrases_train.batch()
    y_phrases_train = np.array(y_phrases_train).ravel()

    x_phrases_test, y_phrases_test = phrases_test.batch()
    y_phrases_test = np.array(y_phrases_test).ravel()

    vectorizer = TfIdfVectorizer(vocab=vocab)
    vectorizer.fit(train, field=train.field('text'))

    embeddings_x_train = vectorizer.transform(x_train)
    embeddings_x_test = vectorizer.transform(x_test)
    embeddings_x_catch = vectorizer.transform(x_catch)
    embeddings_x_phr = vectorizer.transform(x_phrases)

    embeddings_x_phrases_train = vectorizer.transform(x_phrases_train)
    embeddings_x_phrases_test = vectorizer.transform(x_phrases_test)

    pickle.dump(embeddings_x_phrases_train, open("pickled_sets/em_x_phr_train", "wb"))
    pickle.dump(embeddings_x_phrases_test, open("pickled_sets/em_x_phr_test", "wb"))

    pickle.dump(y_phrases_train, open("pickled_sets/y_phr_train", "wb"))
    pickle.dump(y_phrases_test, open("pickled_sets/y_phr_test", "wb"))

    pickle.dump(vectorizer, open("vectorizer", "wb"))
    pickle.dump(vocab, open("vocab", "wb"))

    pickle.dump(embeddings_x_train, open("pickled_sets/em_x_train", 'wb'))
    pickle.dump(embeddings_x_test, open("pickled_sets/em_x_test", 'wb'))
    pickle.dump(embeddings_x_catch, open("pickled_sets/em_x_catch", 'wb'))
    pickle.dump(embeddings_x_phr, open("pickled_sets/em_x_phr", 'wb'))

    pickle.dump(y_train, open("pickled_sets/y_train", "wb"))
    pickle.dump(y_test, open("pickled_sets/y_test", "wb"))
    pickle.dump(y_catch, open("pickled_sets/y_catch", "wb"))
    pickle.dump(y_phrases, open("pickled_sets/y_phr", "wb"))

    pickle.dump(x_train, open("pickled_sets/train", "wb"))


def countbychar():

    chars = {
        0: "Rachel",
        1: "Ross",
        2: "Chandler",
        3: "Monica",
        4: "Joey",
        5: "Phoebe"
    }

    datasets = [
        open("pickled_sets/y_train", "rb"),
        open("pickled_sets/y_test", "rb"),
        open("pickled_sets/y_phr", "rb")
    ]

    names = ["train", "test", "catch"]

    for name, dataset in zip(names, datasets):
        print(name)
        counts = dict().fromkeys(chars.keys(), 0)
        dataset = pickle.load(dataset)
        for label in dataset:
            tmp = counts[label]
            counts[label] = tmp + 1
        print(counts)
        print()


if __name__ == "__main__":

    # countbychar()

    # create_datasets()

    # embeddings_x_train = pickle.load(open("pickled_sets/em_x_train", "rb"))
    # embeddings_x_test = pickle.load(open("pickled_sets/em_x_test", "rb"))
    # embeddings_x_catch = pickle.load(open("pickled_sets/em_x_catch", "rb"))
    # embeddings_x_phrases = pickle.load(open("pickled_sets/em_x_phr", "rb"))
    #
    # y_train = pickle.load(open("pickled_sets/y_train", "rb"))
    # y_test = pickle.load(open("pickled_sets/y_test", "rb"))
    # y_catch = pickle.load(open("pickled_sets/y_catch", "rb"))
    # y_phrases = pickle.load(open("pickled_sets/y_phr", "rb"))

    # embeddings_x_phr_train = pickle.load(open("pickled_sets/em_x_phr_train", "rb"))
    # embeddings_x_phr_test = pickle.load(open("pickled_sets/em_x_phr_test", "rb"))
    #
    # y_phr_train = pickle.load(open("pickled_sets/y_phr_train", "rb"))
    # y_phr_test = pickle.load(open("pickled_sets/y_phr_test", "rb"))

    # x_train = pickle.load(open("pickled_sets/train", "rb"))

    # test_baselines(y_phrases)

    svm = pickle.load(open('models/polyC5', 'rb'))

    # svm = SVC(kernel="linear", C=10)
    # svm.fit(embeddings_x_train, y_train)
    # pickle.dump(svm, open("models/linearC10", "wb"))

    # pred = svm.predict(embeddings_x_phr_train)
    # evaluate("train", pred, y_phr_train)
    # plot_confusion_matrix(svm, embeddings_x_phr_train, y_phr_train)
    #
    # pred_phr = svm.predict(embeddings_x_phr_test)
    # evaluate("test", pred_phr, y_phr_test)
    # plot_confusion_matrix(svm, embeddings_x_phr_test, y_phr_test)

    # test_baselines(y_phr_train)
    # test_baselines(y_phr_test)

    # matplotlib.pyplot.show()

    # catchphrases interactive

    vocab = pickle.load(open("vocab", "rb"))
    vectorizer = pickle.load(open("vectorizer", "rb"))

    chars = {
        0: "Rachel",
        1: "Ross",
        2: "Chandler",
        3: "Monica",
        4: "Joey",
        5: "Phoebe"
    }

    while True:
        a = input("Sentence? ")
        if a == "exit":
            break
        numericalized = vocab.numericalize(a.split())
        tfidf_a = vectorizer.transform([numericalized])
        res = svm.predict(tfidf_a)
        print(chars[int(res)])
