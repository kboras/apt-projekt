# _Hey, that's my line_ - Speaker Identification and Catchphrase Resolution in Multiparty Dialogue

This paper describes a solution to the speaker identification and catchphrase reolution problem in multiparty dialogue. To tackle speaker
identification, we construct a classifier and examine its behavior in regards to data from the TV show Friends. To resolve catchphrases,
we create another set using paraphrase mining, test it on the constructed SVM, and analyze the results with respects to different characters
and their respective catchphrases.