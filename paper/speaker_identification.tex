% Paper template for TAR 2020
% (C) 2014 Jan Šnajder, Goran Glavaš, Domagoj Alagić, Mladen Karan
% TakeLab, FER

\documentclass[10pt, a4paper]{article}

\usepackage{tar2020}

\usepackage[utf8]{inputenc}
\usepackage[pdftex]{graphicx}
\usepackage{booktabs}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{enumitem}
\usepackage{float}

\title{\textit{Hey, that's my line!} --- Speaker Identification and Catchphrase Resolution in Multiparty Dialogue}

\name{Katarina Boras, Ivana Cvitanović, Daria Vanesa Cvitković} 

\address{
University of Zagreb, Faculty of Electrical Engineering and Computing\\
Unska 3, 10000 Zagreb, Croatia\\ 
\texttt{\{katarina.boras, ivana.cvitanovic2, daria-vanesa.cvitkovic\}@fer.hr}\\
}
          
         
\abstract{ 
This paper describes a solution to the speaker identification and catchphrase resolution problem in multiparty dialogue. To tackle speaker identification, we construct a support vector machine classifier and examine its behavior in regards to data from the TV show \textit{Friends}. To resolve catchphrases, we create another dataset using paraphrase mining and test it on the constructed SVM. We hypothesize about the perceived and real impact of catchphrases and conclude that to a classifier, catchphrases aren't any more impactful than regular sentences.
}

\begin{document}

\maketitleabstract

\section{Introduction}

Both speaker identification and catchphrase resolution are multi-class classification tasks. The goal of speaker identification is to determine, from a list of characters, which one is most likely to utter the given sentence. The same procedure is done in catchphrase resolution, where a speaker must be assigned to a catchphrase. However, the issue with catchphrase resolution becomes extracting the actual catchphrases. 

In the case of \textit{Friends}, there are six main characters to map to given sentences. The goal of our system is to be able to not only identify the speaker of a line, but also to be able to connect a certain catchphrase to its owner. For example, if we feed the classifier the sentence ``How you doin'?", it should map it to Joey.

The paper also studies the impact of catchphrases. Viewers of the show can certainly map a catchphrase to a character, but the question is whether they're frequent and important enough for a trained classifier to do it.


\section{Related Work}

The majority of previous work regarding speaker identification depends heavily on acoustic features.


\citet{hazen2003integration} present two different approaches to speaker recognition and discuss their strengths and weaknesses. The first one is a text-dependent Gaussian mixture model approach, and the second one is a speaker adaptive automatic speech recognition model. These two models yield good results. Additionally, the authors fused the two stated approaches, which provided them with additional performance improvements.

While many approaches use acoustic features, there are many instances when those features are not present, and for that reason, there is a need to develop a model that works with only textual features.

\citet{mairesse2007using} claim that it is possible to recognize personality from linguistic cues by exploiting textual features from the text. They examine different feature sets and apply regression and ranking models to model the personality of the speaker.

\citet{campbell2006support} exploit the power of support vector machines to classify patterns. By using the kernel that compares sequences of feature vectors to measure similarity, they manage to distinguish the boundary between a speaker and a set of imposters. This approach differs from other models that usually produce the probability distributions of the speaker and others.

\citet{ma2017text} present a model that uses multi-document convolutional neural network (CNN) for speaker identification of characters from the TV show \textit{Friends}. It uses sequential information by concatenating each utterance with the previous two and one subsequent utterance after the global max-pooling layer. Additionally, to boost the model's ability to identify the character, it can optionally take into account only the speaker that is appearing in the scene.


\subsection{Dataset}

As our main dataset, we used a dataset provided by Shilpi Bhattacharyya\footnote{https://github.com/shilpibhattacharyya}. It contains over 90 thousand lines uttered by the 6 main characters of the show throughout all ten seasons. The distribution of lines over the characters is shown in table \ref{tab:dataset} (column E). 

We split the set into train and test sets with a rather big ratio, lead by the thought that since sentences are fairly different from each other, it is best to have as much data as possible in the train set in order to get better results.

The second dataset, the one we used for catchphrase resolution, was created as follows. First, for each character we came up with a certain catchphrase that they are known for. For some characters, it was fairly easy (e.g. Monica's ``I know!"), but some characters either didn't have one-line catchphrases or had ones that change throughout the show. Examples are Phoebe, who doesn't have a specific catchphrase\footnote{One could argue that ``Oh, no." is somewhat of a catchphrase for her, and we did use it, but it's certainly not on the same level as other characters' catchphrases.}, and Chandler, whose ``catchphrase" is emphasizing the verb \textit{to be} in his jokes. Catchphrases used are as follows:

\begin{itemize}[noitemsep]
	\item ``I know!" for Monica,
	\item ``Hi." and ``We were on a break!" for Ross,
	\item ``Nooo!" for Rachel,
	\item ``Oh, no." for Phoebe,
	\item ``Could I \textit{be} any funnier?" for Chandler, and
	\item ``How you doin'?" for Joey.
\end{itemize}

Since the characters utter their catchphrases in a variety of situations, we employed paraphrase mining to construct this dataset.


\begin{table}
\caption{Datasets in regards to characters. E -- number of examples in the main train and test sets, C -- number of examples in catchphrases set mined from the main train and test set, V -- variance in cosine similarity between catchphrases.}
\label{tab:dataset}
\begin{center}
\begin{tabular}{ccccc}
\toprule
Index & Name & E & C & V \\
\midrule
0 & Rachel & 16736, 180 & 100, 14 & 0.0053 \\
1 & Ross   & 16346, 169 & 44, 21 & 0.0077 \\
2 & Monica   & 15098, 148 & 81, 14 & 0.0030 \\
3 & Chandler & 15067, 143 & 100, 8 & 0.0014 \\
4 & Joey & 14936, 157 & 100, 7 & 0.0115 \\
5 & Phoebe & 13330, 144 & 53, 13 & 0.0081 \\
\bottomrule
\end{tabular}
\end{center}
\end{table}

\subsubsection{Paraphrase Mining} \label{par}

We utilize the SBERT\footnote{https://www.sbert.net/} sentence transformer to find paraphrases of the designated catchphrase(s) for each character. This paraphrase mining function compares all the sentences in the main corpus against each other and returns a list of pairs and their similarity scores calculated using cosine similarity. We set up the experiment so that we extract only the similarities of preselected catchphrases with all other phrases in the corpus, for each of the characters.
We also experimentally extract only those paraphrases that have a similarity score greater than 0.5. Finally, we calculate the variance of the similarity scores for each character, shown in table \ref{tab:dataset}, column V. The variance is calculated over the main train set for fairer results.

Distribution over the characters for this set is shown in table \ref{tab:dataset} (column C) for train and test sets respectively. Nature of catchphrases doesn't allow the train and test sets to be very large.

\subsection{System Description}

Our system consists of two parts: speaker identification and catchphrase resolution. 
For speaker identification, we trained a support vector machine (SVM) on our main train set. For catchphrase resolution, we used the dataset created by paraphrase mining to test our trained SVM on. % System is shown on figure \ref{fig:flowchart}.

% :(

%\begin{figure}
%	\centering
%	\includegraphics[width=\linewidth]{slike/flowchart4}
%	\caption{The system.}
%	\label{fig:flowchart}
%\end{figure}


\subsubsection{Preprocessing} 

As the main dataset examples come in the format (sentence, speaker), some preprocessing needed to be done. For tokenization, we simply split the sentences into words and used words as tokens. We numericalized the tokens using Podium\footnote{https://takelab.fer.hr/podium/index.html} and its vocabulary. 
For feature extraction, we used Podium's TF-IDF vectorizer. It transformed our collection of documents into a matrix of TF-IDF features, as well as converting all characters to lowercase.

\subsubsection{Speaker Identification}

For speaker identification, we used a support vector machine (SVM), implemented in the \textit{scikit-learn}\footnote{https://scikit-learn.org/stable/index.html} library, to assign one of 6 main characters to each sentence. We trained several different models, as is explained later in section \ref{sec:eval} and consequently shown in table \ref{tab:cv}. We conclude that the best model we can hope for is an SVM with a polynomial kernel and hyperparameters $C$ and $\gamma$ set to $5$ and \textit{scale} respectively. 

\subsubsection{Catchphrase Resolution} 

To conduct catchphrase resolution, we used the set created with paraphrase mining as described in section \ref{par} and our trained SVM model. We tested the performance of the model on the catchphrases mined from train and test sets respectively, and obtained results shown in table \ref{tab:cv}.

%For catchphrase resolution, we used our trained SVM model. We tested the performance of the model on mined paraphrases described in section \ref{par} and results are shown in table \ref{tab:cv}.

\section{Evaluation} \label{sec:eval}

We used two baselines to compare our models with. First baseline (``random") assigned a random character to each sentence, and the second one (``loudmouth") searched for the most frequent speaker and assigned them to every sentence. Their results are shown in table \ref{tab:cv}. Our best model is considerably better than both baselines.

Cross-validation was done with respect to three parameters: the kernel of the SVM, $C$ for all models, and $\gamma$ for all models except linear. Due to time and resource constraints, we weren't able to conduct an exhaustive grid search with more hyperparameter values, although we recognize that it would have been the best approach here. 

For gamma values, we used \textit{scikit-learn}'s \textit{scale} and \textit{auto} values that adjust to the dataset. They are calculated as shown in equations \ref{eq:auto} and \ref{eq:scale}.

\begin{equation} \label{eq:auto}
	\gamma_{\text{auto}} = \frac{1}{n_{\text{features}}} \approx 3.058 \cdot 10^{-5}
\end{equation}

\begin{equation} \label{eq:scale}
	\gamma_{\text{scale}} = \frac{1}{n_{\text{features}}*\sigma^2(X)} \approx 1.0166
\end{equation}

Confusion matrices are shown in figures \ref{fig:cmtest}, \ref{fig:cmphrtrain}, and \ref{fig:cmphrtest}. 

We display results for both the catchphrases train and test set to showcase what can be seen from both the cross-validation table and the matrices: the catchphrases sets, both train and test, don't yield results as good as the main test set. Besides the unavoidable difference in sizes of the sets, we argue that this is because the impact of catchphrases, at least when it comes to \textit{Friends}, is not in their frequency, but rather in their memorability. They will seem to be linked specifically to a certain character because we as humans take into consideration so many other elements when hearing the catchphrase, from intonation and the situation the character is in, to our own opinion of the character. Joey, for example, said ``How you doin'?" only several times in the show -- and the classifier shows that -- and yet we remember it as his signature line.


\begin{figure*}[!htb]
	\minipage{0.32\textwidth}
	\includegraphics[width=\linewidth]{slike/cm_test.png}
	\caption{Confusion matrix for the main test set.}\label{fig:cmtest}
	\endminipage\hfill
	\minipage{0.32\textwidth}
	\includegraphics[width=\linewidth]{slike/cm_phrases_train.png}
	\caption{Confusion matrix for the catchphrases train set.}\label{fig:cmphrtrain}
	\endminipage\hfill
	\minipage{0.32\textwidth}%
	\includegraphics[width=\linewidth]{slike/cm_phrases_test.png}
	\caption{Confusion matrix for the catchphrases test set.}\label{fig:cmphrtest}
	\endminipage
\end{figure*}



\begin{table*}[!htb]
\caption{Cross-validation results. A -- accuracy, P -- precision, R -- recall. For gamma values: a -- auto, s -- scale.}
\label{tab:cv}
\begin{center}
\begin{tabular}{ccccccccccccccc}
\toprule
\multicolumn{3}{c}{hyperparameters} & \multicolumn{4}{c}{main test set}            & \multicolumn{4}{c}{catchphrases train set}   & \multicolumn{4}{c}{catchphrases test set}    \\ \midrule
kernel   & $C$  & $\gamma$  & A & P & R & F1 & A & P & R & F1 & A & P & R & F1 \\ \midrule
RBF & 1  & s & 0.795 & 0.799 &	0.794 &	0.796 &	0.712 & 0.723 & 0.712 & 0.711  & 0.494 & 0.494 & 0.502 & 0.480 \\ 
RBF & 1  & a &  0.191 & 	0.0319	& 0.167 & 	0.0535 & \multicolumn{4}{c}{---} & \multicolumn{4}{c}{---}  \\
RBF & 5  & s &  0.865 &	0.869   & 	0.864    &	0.866 &	0.778 & 0.786 & 0.778 & 0.778  & 0.597 & 0.622 & 0.619 & 0.605 \\ 
RBF & 10 & s & 0.864 &	0.869 &   	0.863 &   	0.865 &	0.78 & 0.787 & 0.779 & 0.779 & 0.597 & 0.622 & 0.619 & 0.605 \\ 
RBF & 10 & a & 0.191	& 0.0318 &   	0.167 &   	0.054 &  \multicolumn{4}{c}{---} & \multicolumn{4}{c}{---}  \\ 
RBF & 12 & s & 0.864 &	0.869 &   	0.863 &   	0.865 & 0.78 & 0.787 & 0.78 & 0.78 &  0.597 & 0.622 & 0.619 & 0.605 \\ 
linear & 1  & --- &  0.500 &	0.502 &   	0.497 &   	0.498 &	0.442 & 0.463 & 0.442 & 0.44 &  0.377 & 0.37 & 0.367 & 0.349  \\ 
linear & 5  & --- &  0.623 &	0.633 &   	0.619 &   	0.623 &	0.48 & 0.501 & 0.48 & 0.478 & 0.377 & 0.383 & 0.375 & 0.362 \\ 
linear & 10 & --- &  0.677 &	0.687 &     	0.674 &	0.678 &	0.49 & 0.503 & 0.49 & 0.487 & 0.429 & 0.448 & 0.443 & 0.417 \\ 
poly & 5  & s & \textbf{0.866} &	\textbf{0.871} &   	\textbf{0.865} &   	\textbf{0.867} &	0.778 & 0.786 & 0.778 & 0.778 &  \textbf{0.597} &\textbf{ 0.622} & \textbf{0.619} & \textbf{0.605}  \\ 
\midrule
\multicolumn{3}{c}{random} &   0.181	& 0.181 &   	0.180 &   	0.180 &0.17 & 0.17 & 0.17 & 0.17  & 0.104 & 0.089 & 0.114 & 0.098 \\ 
\multicolumn{3}{c}{loudmouth} & 0.191 &	0.0318 & 	0.167 &   	0.0535 &	0.167 & 0.028 & 0.167 & 0.048  & 0.273 & 0.045 & 0.167 & 0.071 \\ 
\bottomrule
\end{tabular}
\end{center}
\end{table*}


\subsection{Statistical Significance Testing}

To test our hypothesis about the overstated impact of catchphrases, we used statistical significance testing and SciPy's\footnote{https://docs.scipy.org/doc/scipy/reference/index.html} \textit{stats} module.

The null hypothesis was that our two populations (the accuracies obtained from the test set and phrases train set, respectively) are equal. Before testing the hypothesis, we first tested to see whether the two samples are normally distributed, since it influences the methods used in the next step. For this purpose we used two tests: Kolmogorov-Smirnov Normality Test and the Shapiro-Wilk Test. For both tests, we obtained a p-value significantly less than 0.05 for both samples and concluded that they deviate from a normal distribution. Thus, we used non-parametric tests for the next step.

To test our null hypothesis, we used the Mann-Whitney U Test. It yielded a p-value smaller than 0.05, which meant we reject our hypothesis that the two samples are equal; the model has a higher accuracy on the main test set than on the catchphrases train set (and, consequently, the catchphrases test set), as shown on \ref{fig:box} and in \ref{tab:stats}. We can conclude that catchphrases by themselves aren't as impactful to a trained classifier as one viewing the show might expect.

\begin{figure}[!htb]
	\centering
	\includegraphics[width=\linewidth]{slike/box2}
	\caption{Box plot of the two sets. test\_set here refers to the main test set.}
	\label{fig:box}
\end{figure}

\begin{table}[!htb]
	\caption{Results from statistical significance tests. K-S -- Kolmogorov-Smirnov Normality Test, S-W -- Shapiro-Wilk Test, M-W U -- Mann-Whitney U Test.}
	\label{tab:stats}
	\begin{center}
		\begin{tabular}{cccc}
			\toprule
			 & K-S & S-W & M-W U  \\
			\midrule
			Statistic & 0.6914 & 0.6707 & 0.8097 \\
			p & 0.0002   & 0.0004 & 0.0363 \\
			\bottomrule
		\end{tabular}
	\end{center}
\end{table}


\section{Conclusion}

To summarize, we implemented a support vector machine model to tackle speaker identification and catchphrase resolution task. The classifier yielded good results for speaker identification, and somewhat worse results for the other dataset of catchphrases constructed with a paraphrase miner. We argued that this is because catchphrases don't have as much significance as a show viewer might think, and confirm our hypothesis with statistical significance testing.


\section*{Acknowledgements}

We would like to thank Ms. Bhattacharyya for providing the \textit{Friends} dataset we used for this task.

\bibliographystyle{tar2020}
\bibliography{tar2020} 

\end{document}

